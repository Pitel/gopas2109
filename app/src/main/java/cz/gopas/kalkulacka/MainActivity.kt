package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {
	private lateinit var appBarConfiguration: AppBarConfiguration

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
//		if (savedInstanceState == null) {
//			supportFragmentManager.commit {
//				add(android.R.id.content, CalcFragment())
//			}
//		}
		setContentView(R.layout.activity_main)
		Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "Create")

		Log.d(TAG, "${intent.dataString}")

		val navHostFragment =
			supportFragmentManager.findFragmentById(R.id.nav_host) as NavHostFragment
		val navController = navHostFragment.navController
		appBarConfiguration = AppBarConfiguration(navController.graph)
		setupActionBarWithNavController(navController, appBarConfiguration)
	}

	override fun onSupportNavigateUp(): Boolean {
		val navController = findNavController(R.id.nav_host)
		return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
	}

	override fun onResume() {
		super.onResume()
		Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show()
	}

	override fun onPause() {
		Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show()
		super.onPause()
	}

	override fun onStop() {
		Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show()
		super.onStop()
	}

	override fun onDestroy() {
		Toast.makeText(this, "Destroy", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "Destroy")
		super.onDestroy()
	}

	private companion object {
		init {
			FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
		}

		val TAG = MainActivity::class.simpleName
	}
}