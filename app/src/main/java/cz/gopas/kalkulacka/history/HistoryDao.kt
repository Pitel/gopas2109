package cz.gopas.kalkulacka.history

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface HistoryDao {
	@Insert
	fun add(entity: HistoryEntity)

	@Query("SELECT * FROM history ORDER BY id DESC")
	fun getAll(): LiveData<List<HistoryEntity>>
}