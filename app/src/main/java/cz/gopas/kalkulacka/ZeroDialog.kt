package cz.gopas.kalkulacka

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class ZeroDialog : DialogFragment() {
	override fun onCreateDialog(savedInstanceState: Bundle?) =
		MaterialAlertDialogBuilder(requireContext())
			.setMessage(R.string.zerodiv)
			.create()
}