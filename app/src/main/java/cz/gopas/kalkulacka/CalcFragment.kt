package cz.gopas.kalkulacka

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.content.edit
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import cz.gopas.kalkulacka.databinding.FragmentMainBinding
import cz.gopas.kalkulacka.history.HistoryFragment

class CalcFragment : Fragment() {
	private var binding: FragmentMainBinding? = null
	private val viewModel: CalcViewModel by viewModels()

	init {
		setHasOptionsMenu(true)
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		binding = FragmentMainBinding.inflate(inflater, container, false)
		return binding?.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.run {
			viewModel.result.observe(viewLifecycleOwner) {
				res.text = "$it"
			}
			calc.setOnClickListener {
				calc()
			}
			op.setOnCheckedChangeListener { _, _ ->
				calc()
			}
			a.editText?.doAfterTextChanged { calc() }
			b.editText?.doAfterTextChanged { calc() }
			share.setOnClickListener { share() }
			mem.setOnClickListener {
				b.editText?.setText("${viewModel.mem}")
			}
			history.setOnClickListener {
				findNavController().navigate(R.id.history)
			}
			findNavController().currentBackStackEntry?.savedStateHandle
				?.getLiveData<Float>(HistoryFragment.HIST_KEY)
				?.observe(viewLifecycleOwner) {
					a.editText?.setText("$it")
				}
		}
	}

	private fun calc() {
		binding?.run {
			viewModel.calc(
				a.editText?.text?.toString()?.toFloatOrNull() ?: Float.NaN,
				b.editText?.text?.toString()?.toFloatOrNull() ?: Float.NaN,
				op.checkedRadioButtonId
			)
		}
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		inflater.inflate(R.menu.menu, menu)
	}

	override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
		R.id.about -> {
			Log.i(TAG, "About menu")
			findNavController().navigate(R.id.about)
			true
		}
		else -> super.onOptionsItemSelected(item)
	}

	private fun share() {
		Log.d(TAG, "Share!")
		val intent = Intent(Intent.ACTION_SEND)
			.setType("text/plain")
			.putExtra(Intent.EXTRA_TEXT, getString(R.string.share, binding?.res?.text))
		startActivity(intent)
	}

//	override fun onSaveInstanceState(outState: Bundle) {
//		super.onSaveInstanceState(outState)
//		outState.putCharSequence(RES_KEY, binding?.res?.text)
//	}
//
//	override fun onViewStateRestored(savedInstanceState: Bundle?) {
//		super.onViewStateRestored(savedInstanceState)
//		binding?.res?.text = savedInstanceState?.getCharSequence(RES_KEY)
//	}

	override fun onDestroyView() {
		binding = null
		super.onDestroyView()
	}

	private companion object {
		val TAG = CalcFragment::class.simpleName
	}
}