package cz.gopas.kalkulacka

import android.app.Application
import android.util.Log
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import androidx.room.Room
import cz.gopas.kalkulacka.history.HistoryDatabase
import cz.gopas.kalkulacka.history.HistoryEntity
import kotlin.concurrent.thread

class CalcViewModel(app: Application) : AndroidViewModel(app) {
	private val _result = MutableLiveData<Float>()
	val result: LiveData<Float> = _result

	private val prefs by lazy { PreferenceManager.getDefaultSharedPreferences(app) }
	private val db = Room.databaseBuilder(app, HistoryDatabase::class.java, "history")
		//.allowMainThreadQueries()
		.build()
		.historyDao()

	var mem: Float
		get() = prefs.getFloat(RES_KEY, 0f)
		private set(value) = prefs.edit {
			putFloat(RES_KEY, value)
		}

	fun calc(a: Float, b: Float, @IdRes op: Int) {
		thread {
			Log.d(TAG, "Calc!")
			when (op) {
				R.id.add -> a + b
				R.id.sub -> a - b
				R.id.mul -> a * b
				R.id.div -> {
//				if (b == 0f) {
//					findNavController().navigate(R.id.zerodiv)
//				}
					a / b
				}
				else -> Float.NaN
			}.also {
				mem = it
				_result.postValue(it)
				if (!it.isNaN()) {
					db.add(HistoryEntity(it))
				}
			}
		}
	}

	private companion object {
		val TAG = CalcViewModel::class.simpleName
		const val RES_KEY = "res"
	}
}